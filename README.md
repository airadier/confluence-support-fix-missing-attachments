## Confluence Support - Fix Missing Attachments
This is an attachment fixing tool, designed to fix and repair attachments that are missing on disk in Confluence. Since attachment IDs do not change during page moves and are used as a part of the directory structure, we can determine where an attachment should be from the database, and where it currently is from the filesystem.

## Disclaimer
Ensure Confluence has been shut down, and you have a full file system backup
Do not run this against production - run it against a test server first!

### Instructions - Confluence 5.7 and above:
Run the following SQL against your system:

    SELECT spaceid, pageid, contentid
    FROM content
    WHERE contenttype = 'ATTACHMENT'
    AND prevver IS NULL
    and spaceid IS NOT NULL
    
You can alternatively limit to a specific space by name...

    SELECT c.spaceid, c.pageid, c.contentid
    FROM content c
    JOIN SPACES s ON s.spaceid = c.spaceid
    WHERE c.contenttype = 'ATTACHMENT'
    AND c.prevver IS NULL
    AND c.spaceid IS NOT NULL
    AND s.spacename = 'Demonstration Space'
    
... or by space key:

    SELECT c.spaceid, c.pageid, c.contentid
    FROM content c
    JOIN SPACES s ON s.spaceid = c.spaceid
    WHERE c.contenttype = 'ATTACHMENT'
    AND c.prevver IS NULL
    AND c.spaceid IS NOT NULL
    AND s.spacekey = 'DS'

## Attachments.txt File
Once you have the results, export them into a text file named `Attachments.txt` - the name and case sensitivity is important.

As of v1.6, the script will prompt you for the seperator you choose to export with. Type `t` character to use a tab, otherwise, type the character you're using such as `,` or `|`. That character will be used to split each line.

## Running the script
Run `python FindMissingAttachments.py` and follow the prompts.

## Need a hand?
Problems? Attach the `*.txt` files in this directory to your support ticket, and your engineer will be able to help you out. You can also raise an issue here!