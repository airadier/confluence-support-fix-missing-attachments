# -*- coding: utf-8 -*-

import codecs
import os
import shutil
import sys
from distutils.dir_util import copy_tree, remove_tree

print "Confluence Attachment Fixing Tool v1.6.1 - Dave Norton [Atlassian]"
print "Please ensure Confluence has been shut down, and you have a full backup of the home directory"
print ""

# Grab our attachment path
attachmentPath = raw_input('Where is your Confluence attachments directory: ')

# Let's check what the seperator is?
colSeperator = raw_input('What is your column seperator? Type t to use a tab character: ')
if colSeperator == 't':
    print 'Setting your column seperator to a tab character'
    colSeperator = '\t'
else:
    print 'Setting your column seperator to "%s"' % colSeperator

# Clear out the files we're going to write to so we have a clean slate:
open('MissingFiles.txt', 'w').close()
open('PathsToCheck.txt', 'w').close()
open('FoundFiles.txt', 'w').close()

# If there's a trailing slash, get rid of it:
if attachmentPath.endswith('/'):
    attachmentPath = attachmentPath[:-1]

# Check the attachments directory is acceptable:
if not (os.path.isdir(os.path.join(attachmentPath, 'ver003'))):
    print '[Error] - The directory "%s" is not a valid attachments directory' % attachmentPath
    sys.exit(1)

# Make sure our required files are present:
for required_file in ['Attachments.txt']:
    if not os.path.exists(required_file):
        print '[Error] Could not find %s' % required_file
        sys.exit(1)

# This is our mod function
def mod(the_id):
    the_id = int(the_id)
    firstFolder = int(the_id % 250)
    secondFolder = int(((the_id - (the_id % 1000)) / 1000) % 250)
    return os.path.join(str(firstFolder), str(secondFolder), str(the_id))

# a Function for spitting a list to disk
def listtodisk(thelist, filename):
    with open(filename, 'w') as F:
        for item in thelist:
            F.write("%s\n" % item)
        F.close()
    print ""


# a function for zapping the Byte Order Marker
# based on http://stackoverflow.com/questions/8898294/convert-utf-8-with-bom-to-utf-8-with-no-bom-in-python
# as well as https://github.com/terencez127/BomSweeper
def bomzap(filename):
    BUFSIZE = 4096
    BOMLEN = len(codecs.BOM_UTF8)

    with open(filename, 'r+b') as fp:
        chunk = fp.read(BUFSIZE)
        if chunk.startswith(codecs.BOM_UTF8):
            i = 0
            chunk = chunk[BOMLEN:]
            while chunk:
                fp.seek(i)
                fp.write(chunk)
                i += len(chunk)
                fp.seek(BOMLEN, os.SEEK_CUR)
                chunk = fp.read(BUFSIZE)
            fp.seek(-BOMLEN, os.SEEK_CUR)
            fp.truncate()

# An array to store our paths to check:
pathsToCheck = []

# Zap the BOM (if any) in Attachments.txt
print 'Cleaning up the Byte Order Marker in Attachments.txt if any'
bomzap('Attachments.txt')

# Let's generate some paths!
print 'Generating paths based on IDs in Attachments.txt'
with open('Attachments.txt') as f:
    for i, line in enumerate(f):
        # A minimum viable line has a length of 5 - 3 single digits and two 1 character seperators:
        if len(line) < 5:
            print '[INFO] Line %s appears to be an empty or invalid line - ignoring it!' % (i+1)
            continue

        # Split the line (if we can):
        ids = line.strip().split(colSeperator)

        if len(ids[0]) == 0 or len(ids[1]) == 0 or len(ids[2]) == 0:
            sys.exit('[ERROR] Line %s has an empty value for an ID (which will fail). Please delete this line from Attachments.txt' % (i+1))

        # Check if the line matches the spec:
        if len(ids) != 3:
            sys.exit('[ERROR] Line %s does not conform to the spec - spaceid%spageid%scontentid' % (i+1, colSeperator, colSeperator))
        
        # Is the line a header row?
        if ids[0].isalpha() or ids[1].isalpha() or ids[2].isalpha():
            sys.exit('[ERROR] Line %s looks like a header row' % (i+1))

        # Create a path based on each id:
        space = ids[0]
        page = ids[1]
        content = ids[2]

        # Whip up a path:
        pathsToCheck.append(
            os.path.join(attachmentPath, 'ver003', mod(space), mod(page), content))

# Output paths to check to a file:
print 'Writing paths to check to PathsToCheck.txt'
listtodisk(pathsToCheck, 'PathsToCheck.txt')

# Next, checking to see if those files exist on disk:
print 'Performing a check on the files listed in PathsToCheck.txt'
missingFiles = []
with open('PathsToCheck.txt') as f:
    for line in f:
        line = line.strip()
        if not os.path.exists(line):
            missingFiles.append(line)
print 'There are %s missing files' % len(missingFiles)

# Save the missing files to disk:
if len(missingFiles) > 0:
    print 'Writing the missing file paths to MissingFiles.txt'
    listtodisk(missingFiles, 'MissingFiles.txt')

# Should we attempt to fix things?
if len(missingFiles) > 0:
    tocontinuefind = raw_input(
        'This tool can attempt to find and recover the missing files. Would you like to do this now? (y/n): ')

    if tocontinuefind.lower() == 'y':
        print('Building a hash of your attachments directory. This will take a while - stand by!')

        possiblePaths = {}
        foundfiles = []

        for root, dirs, files in os.walk(attachmentPath):
            if len(dirs) > 0:
                # We don't want this - continue:
                continue

            # This is a valid bottom part of the tree - add it to our possible paths:
            possiblePaths[root.split(os.sep)[-1]] = root

        print('Hash table complete - {0} paths in total. Searching for lost attachments....').format(len(possiblePaths))

        # Now that we have our hashtable, let's lookup the missing attachments
        for path in missingFiles:
            attachmentID = path.split(os.sep)[-1]
            if attachmentID in possiblePaths:
                foundfiles.append('%s|%s' % (possiblePaths[attachmentID], path))

        # Spit that out to a file:
        listtodisk(foundfiles, 'FoundFiles.txt')

        print('Found {0} lost attachments - saved them to FoundFiles.txt. Attempting recovery...').format(
            len(foundfiles))
        recoveryCount = 0

        for index, comboPath in enumerate(foundfiles):
            # Get our source and destination:
            source = comboPath.split('|')[0]
            destination = comboPath.split('|')[1]

            # Attempt to make the destination if it does not exist:
            if not os.path.exists(destination):
                os.makedirs(destination)

            # Success flag:
            success = True

            try:
                # Try to copy the tree across (safer):
                copy_tree(os.path.normpath(source), os.path.normpath(destination), preserve_mode=True)
            except shutil.Error as e:
                success = False
                print('Directory not copied. Error: {0}').format(e)

            except OSError as e:
                success = False
                print('Directory not copied. Error: {0}').format(e)
            else:
                # Try and remove the source directory if we copied it successfully:
                remove_tree(source)

                # Increment the recovery counter
                recoveryCount = recoveryCount + 1

            # Define our successText
            successText = 'Success!' if success else 'Failure :('
            print ('Recovering File {0} of {1}: Moving ... {2}').format((index + 1), len(foundfiles), successText)

        # We've completed!
        print('The process has completed. {0} attachments of {1} have been successfully recovered.').format(
            recoveryCount,
            len(foundfiles))

else:
    print 'Your attachments are healthy and in line with the database definitions!'
